create DATABASE IF NOT EXISTS `auto_trading`;

create TABLE IF NOT EXISTS `auto_trading`.`telegram` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `message` TEXT,
    `time` TEXT,
    `is_completed` BOOLEAN
);

create TABLE IF NOT EXISTS `auto_trading`.`commodities` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `COMMODITY` TEXT,
    `PRICE` TEXT,
    `CHANGE` TEXT,
    `% CHG` TEXT,
    `time` TEXT,
    `is_completed` BOOLEAN
);


create TABLE IF NOT EXISTS `auto_trading`.`Nse` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Name` TEXT,
    `CURRENT VALUE` TEXT,
    `CHANGE` TEXT,
    `% CHG` TEXT,
    `OPEN` TEXT,
    `HIGH` TEXT,
    `LOW` TEXT,
    `time` TEXT,
    `is_completed` BOOLEAN
);

create TABLE IF NOT EXISTS `auto_trading`.`Bse` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Name` TEXT,
    `CURRENT VALUE` TEXT,
    `CHANGE` TEXT,
    `% CHG` TEXT,
    `OPEN` TEXT,
    `HIGH` TEXT,
    `LOW` TEXT,
    `time` TEXT,
    `is_completed` BOOLEAN
);
