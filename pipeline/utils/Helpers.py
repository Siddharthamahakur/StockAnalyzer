import asyncio
import base64
import datetime
import glob
import os
import smtplib
import ssl
import urllib.error
import urllib.request
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import bs4
import datacompy
import pandas as pd
from Crypto.Cipher import AES
from pretty_html_table import build_table
from sqlalchemy import create_engine
from telethon.sync import TelegramClient

from pipeline.constants.AppConstants import SECRET_KEY
from pipeline.properties.PropertiesReader import log
from pipeline.validation.Checks import checkUserAuthorization, checkNumberOfDays

logger = log.getLogger(__name__)


def getMySQLEngine(dbConfig):
    user = dbConfig['user']
    password = dbConfig['password']
    host = dbConfig['host']
    port = dbConfig['port']
    dbname = dbConfig['dbname']

    engine = create_engine(
        'mysql://{user}:{password}@{host}:{port}/{dbname}?charset=utf8'.format(
            user=user,
            password=password,
            host=host,
            port=port,
            dbname=dbname
        ), pool_recycle=3600, encoding='utf-8'
    )
    return engine


def writeDataToDatabase(df, table, dbConfig):
    con = getMySQLEngine(dbConfig)
    df.to_sql(table, con=con, index=False, if_exists='append')
    con.dispose()
    logger.info('Data Successfully Inserted to db...')


def executeSelectQuery(sql, dbConfig):
    con = getMySQLEngine(dbConfig)
    df = pd.read_sql(sql, con=con)
    con.dispose()
    return df


def executeUpdateQuery(sql, dbConfig):
    conn = None
    try:
        conn = getMySQLEngine(dbConfig).connect()
        conn.execute(sql)
    except conn.Exception as err:
        logger.exception(err)


def readCsvFile(file):
    df = pd.read_csv(file, encoding='utf-8', sep=',', escapechar='\\')
    return df.astype(str).apply(lambda x: x.str.encode('ascii', 'ignore').str.decode('ascii'))


def moveFiles(src, tgt, pattern):
    if os.path.isdir(src) and os.path.isdir(tgt):
        fileList = glob.glob(os.path.join(src, pattern))
        if len(fileList) > 0:
            for file in fileList:
                os.rename(file, tgt + os.path.basename(file))
            logger.info('Files moved to processed folder')
    else:
        logger.exception("Path doesn't exits")


def writeToCsvFile(data, path):
    df = pd.DataFrame(data)
    df.to_csv(path, index=False, encoding='utf-8', sep=',', escapechar='\\')


def createIfNotExits(fileName):
    directory = os.path.dirname(fileName)
    if not os.path.exists(directory):
        os.makedirs(directory)
    return fileName


def getDeltaRecords(df1, df2):
    compare = datacompy.Compare(
        df1,
        df2,
        join_columns='time',
        df1_name='original',
        df2_name='new'
    )
    compare.matches(ignore_extra_columns=False)
    return compare.df1_unq_rows


def getSoupObject(url):
    soup = None
    try:
        page = urllib.request.urlopen(url, timeout=10)
        pageText = page.read().decode('utf-8')
        soup = bs4.BeautifulSoup(pageText, "html.parser")
    except urllib.error.HTTPError as http_error:
        logger.error(f'HTTPError: {http_error.code} for {url}')
    except urllib.error.URLError as url_error:
        logger.error(f'URLError: {url_error.reason} for {url}')
    else:
        logger.info(f'{url} is up')
    return soup


def getChatMessages(phone_number, api_id, api_hash, channel_name, number_of_messages, number_of_days):
    message = []
    time = []
    messageId = []
    sender = []
    replyTo = []
    counter = 0
    data = {'message': message, 'time': time}
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    client = TelegramClient(phone_number, api_id, api_hash, loop=loop)
    try:
        client.connect()
        client = checkUserAuthorization(client, phone_number)
        chats = client.get_messages(channel_name, number_of_messages)
        if len(chats):
            for chat in chats:
                if checkNumberOfDays(chat.date) == -int(number_of_days):
                    messageId.append(chat.id)
                    message.append(chat.message)
                    sender.append(chat.from_id)
                    replyTo.append(chat.reply_to_msg_id)
                    time.append(chat.date)
                    counter += 1
        logger.info(f'{number_of_days} days => total {counter} chat messages')
    except Exception as err:
        logger.exception(err)
    finally:
        client.disconnect()
    return data


def decode(secret_text):
    cipher = AES.new(SECRET_KEY.encode("utf-8"), AES.MODE_ECB)
    decoded = cipher.decrypt(base64.b64decode(secret_text.encode("utf-8"))).decode("utf-8")
    return decoded.strip()


def sendEmailNotification(df, gmailConfig):
    # https://www.google.com/settings/security/lesssecureapps
    user = decode(gmailConfig['user'])
    password = decode(gmailConfig['password'])
    sender = decode(gmailConfig['user'])
    recipients = decode(gmailConfig['user'])
    smtpServer = gmailConfig['smtpserver']
    port = int(gmailConfig['port'])

    msg = MIMEMultipart()
    msg['Subject'] = "Trading-Report-" + datetime.datetime.now().strftime("%d %B %I:%M")
    msg['From'] = sender
    msg['To'] = recipients

    body_content = build_table(df, 'grey_dark')
    msg.attach(MIMEText(body_content, "html"))
    msg_body = msg.as_string()
    context = ssl.create_default_context()
    try:
        server = smtplib.SMTP(smtpServer, port)
        server.set_debuglevel(0)
        server.ehlo()
        server.starttls(context=context)
        server.ehlo()
        server.login(user, password)
        server.sendmail(msg['From'], msg['To'], msg_body)
        server.quit()
        logger.info(f"Mail Sent to {recipients}")
    except smtplib.SMTPException as err:
        logger.exception(err)
