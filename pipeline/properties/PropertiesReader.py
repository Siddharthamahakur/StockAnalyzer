import configparser
import os
import sys

from pipeline.constants import AppConstants
from pipeline.logger.Logging import log

log = log()
logger = log.getLogger(__name__)


def configReader(section):
    try:
        configItems = None
        configDir = AppConstants.ROOT + AppConstants.RESOURCES_DIR
        directory = os.path.dirname(configDir)
        if not os.path.exists(directory):
            os.makedirs(directory)
        config = configparser.ConfigParser()
        files = [configDir + 'application.ini']
        config.read(files)
        if len(config.read(files)) != len(files):
            raise ValueError("Failed to open/find all config files")
        for sect in config.sections():
            if sect == section:
                configItems = config.items(sect)
                break
        return configItems
    except configparser.MissingSectionHeaderError:
        raise SyntaxError("Missing section header")
    except Exception as err:
        logger.exception(str(err), "Oops!", sys.exc_info()[0], "occurred.")
