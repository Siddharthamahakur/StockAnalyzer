# Web scraping
name = []
low = []
opEn = []
high = []
change = []
commodity = []
price = []
lastPrice = []
prevClose = []
companyName = []
currentValue = []
percentageChg = []
percentageLoss = []
fiveDayPerformance = []
baseURL = "http://www.moneycontrol.com"
bseNSeColumns = ['Name', 'CURRENT VALUE', 'CHANGE', '% CHG', 'OPEN', 'HIGH', 'LOW']
commoditiesColumns = ['COMMODITY', 'PRICE', 'CHANGE', '% CHG']

url = {
    'indianIndicesURL': 'http://www.moneycontrol.com/markets/indian-indices/',
    'baseUrl': 'http://www.moneycontrol.com/'
}
xpath = {
    'liveMarket': '//*[@id="mc_content"]/div/div/div[2]/section[2]/ul/li[2]/a',
    'base': '//*[@id="nsebse_2"]/div[1]/ul/li[1]/a',
    'nse': '//*[@id="nsebse_2"]/div[1]/ul/li[2]/a',
    'mcx': '//*[@id="mc_mainWrapper"]/section/div/div[5]/div[1]/div[1]/ul/li[1]/a'
}

table = {
    'mcxTable': 'PR tab-pane in active fade',
    'webpageTime': 'tableheading TAR'
}
