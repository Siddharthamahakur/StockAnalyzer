import os

DATA_DIR = '/data/'
RAW_DATA_DIR = 'raw/'
PROCESSED_DATA_DIR = 'processed/'
RESOURCES_DIR = '/configs/'
LOGGING_DIR = '/log/auto-trading.txt'
ROOT = os.path.dirname(os.path.realpath(__file__)).rsplit(os.sep, 2)[0]

# Telegram Configuration
NUM_OF_DAY = '4'
TELEGRAM = 'TELEGRAM'
GMAIL = 'GMAIL'
MYSQL = 'MYSQL'
SQL = 'SQL'
SECRET_KEY = 'e5ZHEkoijfq9PGhFueHPELDFxIcgGVQC'

TWITTER = 'TWITTER'
