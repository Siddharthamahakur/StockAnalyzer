import os
import re
from datetime import datetime

import pandas as pd
from lxml import etree
from tabulate import tabulate

from pipeline.constants.MoneyControlConstants import *
from pipeline.properties.PropertiesReader import log
from pipeline.utils import Helpers

logger = log.getLogger(__name__)


def getBseNSeData(tableName, exchangeName, dbConfig):
    tableRows = tableName[0].find_all("tr")
    tableRows = tableRows[1:]

    for row in tableRows:
        columns = row.find_all("td")
        name.append(columns[0].text)
        currentValue.append(columns[1].text)
        change.append(columns[2].text)
        percentageChg.append(columns[3].text)
        opEn.append(columns[4].text)
        high.append(columns[5].text)
        low.append(columns[6].text)

    df = pd.DataFrame(list(zip(name, currentValue, change, percentageChg, opEn, high, low)), columns=bseNSeColumns)
    df['time'] = pd.to_datetime('now')
    print(f'{exchangeName} => ' + os.linesep + tabulate(df, tablefmt="fancy_grid"))
    Helpers.writeDataToDatabase(df, f'{exchangeName}', dbConfig)


def marketsIndianIndices(dbConfig):
    soup = Helpers.getSoupObject(url['indianIndicesURL'])
    webpageTime = str(soup.find_all("div", table['webpageTime']).pop(1))
    webpageTime = webpageTime[webpageTime.find(">") + 1: webpageTime.find("</")]
    webpageTime = datetime.strptime(webpageTime, "%d %b %I:%M")
    logger.info(f'indian-indices Time - {webpageTime}')

    dom = etree.HTML(str(soup))
    liveMarket = dom.xpath(xpath['liveMarket'])[0].text
    base = dom.xpath(xpath['base'])[0].text
    nse = dom.xpath(xpath['nse'])[0].text

    bseTable = soup.find_all("div", "clearfix tab-pane fade")
    nseTable = soup.find_all("div", "clearfix tab-pane fade in active")

    if liveMarket == 'Live Markets' and base == 'Bse' and nse == 'Nse':
        getBseNSeData(bseTable, base, dbConfig)
        getBseNSeData(nseTable, nse, dbConfig)


def commodities(dbConfig):
    soup = Helpers.getSoupObject(url['baseUrl'])
    dom = etree.HTML(str(soup))
    mcx = dom.xpath(xpath['mcx'])[0].text
    mcxTable = soup.find_all('div', table['mcxTable'])

    if re.sub(r"[\n\t\s]*", "", mcx) == 'MCX':
        tableRows = mcxTable[0].find_all("tr")
        tableRows = tableRows[1:]
        for row in tableRows:
            columns = row.find_all("td")
            commodity.append(re.sub(r"[\n\t]*", "", columns[0].text))
            price.append(re.sub(r"[\n\t]*", "", columns[1].text))
            change.append(re.sub(r"[\n\t]*", "", columns[3].text))
            percentageChg.append(re.sub(r"[\n\t]*", "", columns[4].text))

    df = pd.DataFrame(list(zip(commodity, price, change, percentageChg)), columns=commoditiesColumns)
    df['time'] = pd.to_datetime('now')
    print('commodities => ' + os.linesep + tabulate(df, tablefmt="fancy_grid"))
    Helpers.writeDataToDatabase(df, 'commodities', dbConfig)


def run(dbConfig):
    marketsIndianIndices(dbConfig)
    commodities(dbConfig)
