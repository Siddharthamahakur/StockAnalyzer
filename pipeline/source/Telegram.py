import datetime

from sqlalchemy import text

from pipeline.constants.AppConstants import ROOT, DATA_DIR, RAW_DATA_DIR, PROCESSED_DATA_DIR
from pipeline.properties.PropertiesReader import log
from pipeline.utils import Helpers
from pipeline.validation.Checks import checkStockLose

logger = log.getLogger(__name__)


def run(telegramConfig, gmailConfig, sqlConfig, dbConfig, number_of_days, dbName):
    api_id = int(telegramConfig['api_id'])
    api_hash = telegramConfig['api_hash']

    phone_number = telegramConfig['phone_number']
    logger.info(f'phone_number - {phone_number}')

    channel_name = telegramConfig['channel_name']
    logger.info(f'Channel Name - {channel_name}')

    number_of_messages = int(telegramConfig['number_of_messages'])
    logger.info(f'Number of messages to be extracted - {number_of_messages}')

    source = ROOT + DATA_DIR + RAW_DATA_DIR
    logger.info(f'source path - {source}')

    destination = ROOT + DATA_DIR + PROCESSED_DATA_DIR
    logger.info(f'destination path - {destination}')

    filename = source + channel_name + '-' + str(datetime.datetime.now()) + '.csv'
    data = Helpers.getChatMessages(phone_number, api_id, api_hash, channel_name, number_of_messages, number_of_days)

    # Write data to csv file
    Helpers.writeToCsvFile(data, filename)

    # Read data from csv file
    read_csv_df = Helpers.readCsvFile(filename)
    read_csv_df['is_completed'] = 1

    # Read data from database
    read_db_data_df = Helpers.executeSelectQuery(text(sqlConfig['select_is_completed_telegram_sql']), dbConfig)

    # Compare file data with database data
    compare_data_df = Helpers.getDeltaRecords(read_csv_df, read_db_data_df)

    if compare_data_df.size > 0:
        logger.info(f'{compare_data_df.size} new messages available')

        # updating flag value
        Helpers.executeUpdateQuery(text(sqlConfig['update_is_completed_telegram_sql']), dbConfig)

        # Insert records to table
        logger.info(f'database name : {dbName}')
        Helpers.writeDataToDatabase(compare_data_df, dbName, dbConfig)

        # Validate SL data
        validate_SL_df = checkStockLose(compare_data_df)

        # Send Email
        if validate_SL_df.size > 0:
            Helpers.sendEmailNotification(validate_SL_df, gmailConfig)
            Helpers.moveFiles(source, destination, filename)
        # End

    else:
        logger.warning('No updated messages available for insert')
