from datetime import datetime


def checkNumberOfDays(chat_date):
    message_date = datetime.strptime(str(chat_date).split()[0], '%Y-%m-%d')
    today = datetime.today().strftime('%Y-%m-%d')
    days = (message_date - datetime.strptime(today, '%Y-%m-%d')).days
    return days


def checkUserAuthorization(client, phone_number):
    if not client.is_user_authorized():
        client.send_code_request(phone_number)
        client.sign_in(phone_number, input('Enter the code: '))
    return client


def checkStockLose(df):
    new_df = df[df[['message']].apply(lambda x: x.str.contains('\@\w+\s', regex=True)).any(axis=1)]
    new_df = new_df.replace({r'\s+$': '', r'^\s+': ''}, regex=True).replace(r'\n', ' ', regex=True)
    return new_df.rename({'message': 'Stock Summary'}, axis=1)
