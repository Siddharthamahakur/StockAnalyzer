import logging
import os
import sys
from logging.handlers import TimedRotatingFileHandler

from pipeline.constants import AppConstants


def log():
    # Change root log level from WARNING (default) to NOTSET in order for all messages to be delegated.
    logging.getLogger().setLevel(logging.NOTSET)

    # Add stdout handler, with level INFO
    console = logging.StreamHandler(sys.stdout)
    console.setLevel(logging.INFO)
    Format = "[%(levelname).4s] %(asctime)s %(lineno).4d:%(name)-30s - %(message)s"
    fmt = logging.Formatter(Format, datefmt='%d-%m-%Y %I:%M:%S %p')
    console.setFormatter(fmt)
    logging.getLogger().addHandler(console)

    # Add file rotating handler, with level DEBUG
    fileName = AppConstants.ROOT + AppConstants.LOGGING_DIR
    directory = os.path.dirname(fileName)
    if not os.path.exists(directory):
        os.makedirs(directory)
    rotatingHandler = logging.handlers.RotatingFileHandler(filename=fileName, maxBytes=1000000, backupCount=3)
    rotatingHandler.setLevel(logging.INFO)
    formatter = logging.Formatter(Format)
    rotatingHandler.setFormatter(formatter)
    logging.getLogger().addHandler(rotatingHandler)

    return logging
