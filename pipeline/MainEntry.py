from pipeline.constants.AppConstants import NUM_OF_DAY, TELEGRAM, GMAIL, SQL, MYSQL
from pipeline.properties.PropertiesReader import log, configReader
from pipeline.source import Telegram, MoneyControl

logger = log.getLogger(__name__)
__author__ = "Siddhartha Mahakur"
__credits__ = ["Siddhartha Mahakur"]
__version__ = "1.0.1"
__maintainer__ = "Siddhartha Mahakur"
__email__ = "siddharthmahakur@gmail.com"
__status__ = "develop"

if __name__ == '__main__':
    try:
        logger.info(f"\nAuthor : {__author__}\nVersion : {__version__}\nStatus : {__status__}")
        telegramConfig = dict(configReader(TELEGRAM))
        gmailConfig = dict(configReader(GMAIL))
        sqlConfig = dict(configReader(SQL))
        dbConfig = dict(configReader(MYSQL))
        number_of_days = NUM_OF_DAY
        dbName = TELEGRAM.lower()

        logger.info('Telegram pipeline Execution start')
        Telegram.run(telegramConfig, gmailConfig, sqlConfig, dbConfig, number_of_days, dbName)
        logger.info('Telegram pipeline Execution end')

        logger.info('MoneyControl pipeline Execution start')
        MoneyControl.run(dbConfig)
        logger.info('MoneyControl pipeline Execution end')

    except Exception as ex:
        logger.exception(ex)
