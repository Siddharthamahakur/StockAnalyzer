bs4~=0.0.1
beautifulsoup4~=4.9.3
datacompy~=0.7.2
pandas~=1.1.5
SQLAlchemy~=1.4.21
Telethon~=1.21.1
lxml~=4.6.3
tabulate~=0.8.9