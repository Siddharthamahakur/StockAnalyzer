import base64

from Crypto.Cipher import AES

from pipeline.constants.AppConstants import SECRET_KEY

cipher = AES.new(SECRET_KEY.encode("utf-8"), AES.MODE_ECB)
decoded = cipher.decrypt(base64.b64decode(''.encode("utf-8"))).decode("utf-8")
print(decoded.strip())

encoded = base64.b64encode(cipher.encrypt(''.encode("utf-8").rjust(32))).strip()
print(encoded)
